# Projeto: People of Aristotle


1. Colher as referências de Aristóteles a outras figuras histórias ou fictícias utilizando a numeração Bekker. 
2. Eleger um formato para o sistema de tags (MD, html,  XML, xls). 
3. Organizar as referências a partir de tags, identificando o nome da pessoa, a obra em que ocorre a referência e a origem da pessoa (figura histórica, fictícia, lendária, etc.) 
4. Colher informações adicionais sobre as figuras citadas a partir de outras fontes primárias e da literatura secundária.
