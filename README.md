# ΣΤΑΓΙΡΙΤΗΣ (Stagirites)

O repositório tem o objetivo geral de **colher e organizar materiais relevantes para o estudo da filosofia antiga.**

Objetivos específicos incluem: 
* Organizar referências a recursos em formato Wiki.
* Disponibilizar textos de fontes primárias em formatos editáveis estruturados, como Markdown, LaTex e PDF. 
